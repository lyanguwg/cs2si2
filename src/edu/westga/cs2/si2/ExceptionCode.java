package edu.westga.cs2.si2;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * The class contains code that may throw exceptions
 * @author CS2
 * @version 1.0
 *
 */
public class ExceptionCode {

	/**
	 * The method takes an array list of integers and generates a list of quotients that are created by dividing the integers by 
	 * user supplied numbers.
	 * 
	 * @param values an ArrayList of Integers
	 * @precondition values != null && values.size() !=0
	 * @postcondition none
	 * 
	 */
	public void displayQuotient(ArrayList<Integer> values) {
		
		if (values == null) {
			throw new IllegalArgumentException("list cannot be null");
		}
		if (values.size() == 0) {
			throw new IllegalArgumentException("list cannot be empty");
		}
		try {
			this.generateQuotient(values);
			this.printResults(values);
		} catch (InputMismatchException ex) {
			System.err.println("input mismatch" + ex.getMessage());
		} catch (Exception ex) {
			System.err.println("Houston, we have a problem." + ex.getMessage());
		}

		System.out.println("Size of list is: " + values.size());
	}

	private void generateQuotient(ArrayList<Integer> myList) {
		Scanner input = new Scanner(System.in);

		try {
			for (int index = 0; index < myList.size(); index++) {
				try {
					System.out.print("Enter a divisor: ");
					int divideBy = input.nextInt();
					int result = myList.get(index) / divideBy;
					myList.set(index, result);
				} catch (ArithmeticException ex) {
					System.err.println("Division by zero.");
				}
			}
		} catch (NullPointerException ex) {
			System.err.println("null exception");
			ex.printStackTrace();
		} finally {
			input.close();
		}
	}
	
	private void printResults(ArrayList<Integer> values) {
		if (values == null)  {
			throw new IllegalArgumentException("values cannot be null");
		}
		if (values.size() == 0) {
			throw new IllegalArgumentException("values cannot be empty");
		}
		String result = "(";
		for (Integer value: values) {
			result += value + ",";
			
		}
		result = result.substring(0, result.length() - 1) + ")";
		
		System.out.println(result);
		
	}
}
