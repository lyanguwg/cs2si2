package edu.westga.cs2.si2;

import java.util.ArrayList;
/**
 * The class demonstrate exceptions.
 * @author CS2
 * @version 1.0
 *
 */

public class DemoMain {
	
	/**
	 * The main entry to the application.
	 * @param args the command line arguments.
	 */
	public static void main(String[] args) {
		ExceptionCode demo = new ExceptionCode();
		ArrayList<Integer> values = new ArrayList<Integer>();
		values.add(3);
		values.add(4);
		values.add(5);
		demo.displayQuotient(values);
	}
}
